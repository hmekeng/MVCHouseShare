﻿CREATE FUNCTION [dbo].[NbJoursEchangeBien] (@idBien INT)
RETURNS INT
BEGIN
	DECLARE @NbJours INT
	DECLARE @DateDebEchange DATE
	DECLARE @DateFinEchange DATE

	SET @NbJours = 0

	DECLARE CURBIEN CURSOR
	FOR SELECT DateDebEchange, DateFinEchange FROM MembreBienEchange

	OPEN CURBIEN
	FETCH CURBIEN INTO @DateDebEchange, @DateFinEchange
	
	WHILE (@@FETCH_STATUS=0)
	BEGIN
		SET @NbJours += DATEDIFF(DAY, @DateDebEchange, @DateFinEchange)
		FETCH CURBIEN INTO @DateDebEchange, @DateFinEchange	
	END

	CLOSE CURBIEN
	DEALLOCATE CURBIEN
	
	RETURN @NbJours
END
