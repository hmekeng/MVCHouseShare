﻿CREATE TABLE [dbo].[Membre] (
    [idMembre]  INT            IDENTITY (1, 1) NOT NULL,
    [Nom]       NVARCHAR (50)  NOT NULL,
    [Prenom]    NVARCHAR (50)  NOT NULL,
    [Email]     NVARCHAR (256) NOT NULL,
    [Pays]      INT            NOT NULL,
    [Telephone] NVARCHAR (20)  NOT NULL,
    [Login]     NVARCHAR (50)  NOT NULL,
    [Password]  NVARCHAR (256) NOT NULL,
    [PhotoUser] NCHAR (10)     NULL,
    [isDeleted] BIT            CONSTRAINT [DF_Membre_isDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_membre] PRIMARY KEY CLUSTERED ([idMembre] ASC)
);


GO
CREATE TRIGGER [dbo].[Trig_DelMembre]
   ON  [dbo].[Membre]
  instead of delete
AS 
BEGIN
	
	Declare @datesup date

	set @datesup = GETDATE();
	
--Si l'échange commence après la suppression, on n'en tient pas compte
--Si l'échange se termine avant la suppression, on n'en tient pas compte

	if( exists(select * from MembreBienEchange
				where MembreBienEchange.Valide =1
				and (DateDebEchange<=GETDATE() and DateFinEchange >=GETDATE()
				and idMembre in (select idMembre from deleted)
				)
			  )
	 )
				BEGIN
					RAISERROR (N'Vous ne pouvez pas supprimer un bien dont l''échange est en cours', 
								10,
								1);    
								Rollback;
				END

	ELSE
				BEGIN
					UPDATE MEmbre set isDeleted=1 where idMembre in (select idMembre from deleted)
				END
		
END

