﻿CREATE TABLE [dbo].[HistoAvis] (
    [idAvis]    INT           NOT NULL,
    [DateAvis]  DATETIME      NOT NULL,
    [NoteAvis]  INT           NOT NULL,
    [idMembre]  INT           NOT NULL,
    [idProprio] INT           NOT NULL,
    [TitreBien] NVARCHAR (50) NOT NULL,
    [VilleBien] NVARCHAR (50) NOT NULL,
    [NbEchange] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([idAvis] ASC)
);

