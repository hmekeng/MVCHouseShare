﻿CREATE TABLE [dbo].[AvisMembreBien] (
    [idAvis]   INT      IDENTITY (1, 1) NOT NULL,
    [note]     INT      NOT NULL,
    [message]  NTEXT    NOT NULL,
    [idMembre] INT      NOT NULL,
    [idBien]   INT      NOT NULL,
    [DateAvis] DATETIME NOT NULL,
    [Approuve] BIT      CONSTRAINT [DF_AvisMembreBien_Approuve] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AvisMembreBien] PRIMARY KEY CLUSTERED ([idAvis] ASC),
    CONSTRAINT [FK_AvisMembreBien_BienEchange] FOREIGN KEY ([idBien]) REFERENCES [dbo].[BienEchange] ([idBien]),
    CONSTRAINT [FK_AvisMembreBien_membre] FOREIGN KEY ([idMembre]) REFERENCES [dbo].[Membre] ([idMembre])
);


GO
CREATE TRIGGER [dbo].[TR_CommentaireHistorique]
ON [dbo].[AvisMembreBien]
FOR DELETE
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE name='HistoAvis')  
	CREATE TABLE HistoAvis (idAvis INT PRIMARY KEY,
							DateAvis DATETIME NOT NULL, 
							NoteAvis INT NOT NULL, 
							idMembre INT NOT NULL,
							idProprio INT NOT NULL, 
							TitreBien NVARCHAR(50) NOT NULL, 
							VilleBien NVARCHAR(50) NOT NULL,
							NbEchange INT NOT NULL);
	DECLARE @idAvis INT, @DateAvis DATETIME, @NoteAvis INT, @idMembre INT, @idProprio INT, @TitreBien NVARCHAR(50), @VilleBien NVARCHAR(50), @NbEchange INT

	DECLARE CR_Avis CURSOR
	FOR SELECT D.idAvis, D.DateAvis, D.note, D.idMembre, BE.idMembre, BE.titre, BE.Ville, COUNT(MBE.idBien) AS 'Nb Echange' FROM deleted D 
		JOIN BienEchange BE ON D.idBien = BE.idBien
		JOIN MembreBienEchange MBE ON BE.idBien = MBE.idBien
		GROUP BY D.idAvis, D.DateAvis, D.note, D.idMembre, BE.idMembre, BE.titre, BE.Ville

	OPEN CR_Avis
	FETCH CR_Avis INTO @idAvis, @DateAvis, @NoteAvis, @idMembre, @idProprio, @TitreBien, @VilleBien, @NbEchange

	WHILE (@@FETCH_STATUS = 1)
	BEGIN
		INSERT INTO HistoAvis VALUES (@idAvis, @DateAvis, @NoteAvis, @idMembre, @idProprio, @TitreBien, @VilleBien, @NbEchange)
		FETCH CR_Avis INTO @idAvis, @DateAvis, @NoteAvis, @idMembre, @idProprio, @TitreBien, @VilleBien, @NbEchange
	END

	CLOSE CR_Avis
	DEALLOCATE CR_Avis

END
