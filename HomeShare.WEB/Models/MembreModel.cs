﻿using HomeShare.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeShare.WEB.Models
{
    public class MembreModel
    {
        [Required]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        public string Nom { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        public string Prenom { get; set; }

        [Required]
        [Display(Name = "Adresse EMail")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Telephone")]
       
        public string Telephone { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        public string Login { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        [StringLength(50, ErrorMessage = ":Less than 30 characters")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string PhotoUser { get; set; }

        public int IdPays { get; set; }

        public IEnumerable<Pays> ListPays { get; set; }
    }

  

}