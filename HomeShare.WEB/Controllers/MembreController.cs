﻿using HomeShare.DAL;
using HomeShare.WEB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeShare.WEB.Infra;

namespace HomeShare.WEB.Controllers
{
    public class MembreController : Controller
    {
        // GET: Membre
        private DAOMembre daoUser = new DAOMembre();
        private DAOPays daoPays = new DAOPays();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            MembreModel mbreM = new MembreModel();
            mbreM.ListPays = daoPays.readAll();
            return View(mbreM);
           
        }



        [HttpPost]
        public ActionResult Register(MembreModel Account, HttpPostedFileBase file, int IdPays)
        {

            Membre membre = new Membre();

            membre.Nom = Account.Nom;
            membre.Prenom = Account.Prenom;
     
            membre.Telephone= Account.Telephone;
            membre.Email = Account.Email;
            membre.Login = Account.Login;
            membre.Password = Account.Password;
            membre.IsDeleted = false;


            membre.IdPays = Account.IdPays;

            var allowedExtensions = new[] {
                    ".Jpg", ".png", ".jpg", ".jpeg"
                        };

            membre.PhotoUser= file.ToString(); //acceder a l'url  complet

            var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
            var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
            if (allowedExtensions.Contains(ext)) //permet de verifier l'extension de notre fichier   
            {
                string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                                                                          /*string myfile = name + "_" + ext;*/ //appending the name with id  
                string myfile = name + ext;
                // store the file inside ~/project folder(Img)  
                var path = Path.Combine(Server.MapPath("~/images"), file.FileName);
                membre.PhotoUser = "images/" + myfile;



                file.SaveAs(path);

              
            }
            else
            {
                ViewBag.message = "Please choose only Image file";
            }

            daoUser.create(membre);


            return RedirectToAction("RegisterDashBoard");
        }

        [HttpGet]
        public ActionResult Login()
        {

            return View();
        }
      

        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            Session["admin"] = "";
            Membre user = daoUser.GetUserAccount(login, password);
            if (user != null)
            {
                SessionManage.UserConnected = user;

                Session["membre"] = user.Prenom + " " + user.Nom.Substring(0, 1);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("About", "Home");
            }
        }
 

        public ActionResult Deconnexion()
        {
            Session["userConnected"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}