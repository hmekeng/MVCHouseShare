﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class AvisMembreBien
    {
        public int IdAvis { get; set; }
        public int note { get; set; }
        public string Message { get; set; }
        public Membre Membre { get; set; }
        public BienEchange BienEchange { get; set; }

        public DateTime DateAvis { get; set; }

        public bool? Approuve { get; set; }

        public AvisMembreBien()
        {
        }

        public AvisMembreBien(int note, string message, Membre membre, BienEchange bienEchange, DateTime dateAvis, bool? approuve)
        {
            this.note = note;
            Message = message;
            Membre = membre;
            BienEchange = bienEchange;
            DateAvis = dateAvis;
            Approuve = approuve;
        }

        public AvisMembreBien(int idAvis, int note, string message, Membre membre, BienEchange bienEchange, DateTime dateAvis, bool? approuve)
        {
            IdAvis = idAvis;
            this.note = note;
            Message = message;
            Membre = membre;
            BienEchange = bienEchange;
            DateAvis = dateAvis;
            Approuve = approuve;
        }
    }
}
