﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class BienEchange
    {
        public int IdBien { get; set; }
        public string Titre{ get; set; }
        public string DescCourte{ get; set; }
        public string DescLong { get; set; }
        public int NombrePerson { get; set; }
        public string Ville { get; set; }
        public string Rue { get; set; }
        public string Numero { get; set; }
        public string CodePostal { get; set; }
        public string Photo { get; set; }
        public bool? AssuranceObligatoire { get; set; }
        public bool? IsEnabled{ get; set; }
        public DateTime DisabledDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Pays Pays { get; set; }
        public Membre Membre { get; set; }

        public DateTime DateCreation { get; set; }

        public BienEchange()
        {
        }

        public BienEchange(string titre, string descCourte, string descLong, int nombrePerson, Pays pays, string ville, string rue, string numero, string codePostal, string photo, bool? assuranceObligatoire, bool? isEnabled, DateTime disabledDate, string latitude, string longitude, Membre membre, DateTime dateCreation)
        {
            Titre = titre;
            DescCourte = descCourte;
            DescLong = descLong;
            NombrePerson = nombrePerson;
            Pays = pays;
            Ville = ville;
            Rue = rue;
            Numero = numero;
            CodePostal = codePostal;
            Photo = photo;
            AssuranceObligatoire = assuranceObligatoire;
            IsEnabled = isEnabled;
            DisabledDate = disabledDate;
            Latitude = latitude;
            Longitude = longitude;
            Membre = membre;
            DateCreation = dateCreation;
        }

        public BienEchange(int idBien, string titre, string descCourte, string descLong, int nombrePerson, Pays pays, string ville, string rue, string numero, string codePostal, string photo, bool? assuranceObligatoire, bool? isEnabled, DateTime disabledDate, string latitude, string longitude, Membre membre, DateTime dateCreation)
        {
            IdBien = idBien;
            Titre = titre;
            DescCourte = descCourte;
            DescLong = descLong;
            NombrePerson = nombrePerson;
            Pays = pays;
            Ville = ville;
            Rue = rue;
            Numero = numero;
            CodePostal = codePostal;
            Photo = photo;
            AssuranceObligatoire = assuranceObligatoire;
            IsEnabled = isEnabled;
            DisabledDate = disabledDate;
            Latitude = latitude;
            Longitude = longitude;
            Membre = membre;
            DateCreation = dateCreation;
        }
    }
}
