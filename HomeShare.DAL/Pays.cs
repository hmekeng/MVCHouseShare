﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class Pays
    {
        public int IdPays { get; set; }
     
        public string Libelle { get; set; }
        
        public string Photo{ get; set; }


        public Pays()
        {
        }

        public Pays(string libelle, string photo)
        {
            Libelle = libelle;
            Photo = photo;
        }

        public Pays(int idPays, string libelle, string photo)
        {
            IdPays = idPays;
            Libelle = libelle;
            Photo = photo;
        }
    }


}
