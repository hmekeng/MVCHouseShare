﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class Membre
    {
        public int IdMembre{ get; set; }
        public string Nom { get; set; }
        public string Prenom{ get; set; }
        public string Email{ get; set; }
        public int IdPays { get; set; }
        public string Telephone { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PhotoUser { get; set; }

        public bool? IsDeleted { get; set; }
        

        public Membre()
        {
        }

        public Membre(int idMembre, string nom, string prenom)
        {

            Nom = nom;
            Prenom = prenom;
            IdMembre = idMembre;
         


        }

        public Membre(string nom, string prenom, string email, int idPays, string telephone, string login, string password, string photoUser, bool? isDeleted)
        {
            Nom = nom;
            Prenom = prenom;
            Email = email;
            IdPays = idPays;
            Telephone = telephone;
            Login = login;
            Password = password;
            PhotoUser = photoUser;
            IsDeleted = isDeleted;
        }

        public Membre(int idMembre, string nom, string prenom, string email, int idPays, string telephone, string login, string password, string photoUser, bool? isDeleted) : this(idMembre, nom, prenom)
        {
            Email = email;
            IdPays = idPays;
            Telephone = telephone;
            Login = login;
            Password = password;
            PhotoUser = photoUser;
            IsDeleted = isDeleted;
        }
    }
}
