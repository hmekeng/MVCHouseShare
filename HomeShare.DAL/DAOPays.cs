﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class DAOPays : DAOCon, IDAO<Pays>
    {
        public Pays create(Pays objet)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Pays read(int id)
        {
            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM Pays WHERE  idPays= " + id;
            command.CommandText = query;
            command.Parameters.AddWithValue("idPays", id);
            connection.Open();
            SqlDataReader rdr = command.ExecuteReader();
            Pays pays = null;

            while (rdr.Read())
            {
                pays = new Pays
                {
                    IdPays = (int)rdr["idPays"],
                    Libelle = (string)rdr["Libelle"],
                    Photo = (string)rdr["Photo"],
                  

                };

            }
            rdr.Close();
            connection.Close();
            return pays;
        }



        public List<Pays> readAll()
        {
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM pays";
            command.CommandText = query;
            connection.Close();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<Pays> listPays = new List<Pays>();
            foreach (DataRow row in dt.Rows)
            {
                Pays pays = new Pays(
                   (int)row["idPays"],
                   row["Libelle"].ToString(),
                   row["Photo"].ToString());

                listPays.Add(pays);
            }
            return listPays;
        }

        public Pays update(Pays objet)
        {
            throw new NotImplementedException();
        }
    }
}
