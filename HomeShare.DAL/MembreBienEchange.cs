﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class MembreBienEchange
    {
        public int IdMembreBienEchange { get; set; }
        public int IdMembre { get; set; }
        public int IdBien { get; set; }
        public Membre Membre  { get; set; }
        public BienEchange BienEchange { get; set; }
    
        public DateTime DateDebEchange { get; set; }
        public DateTime DateFinEchange { get; set; }
        public bool? Assurance { get; set; }
        public bool Valide { get; set; }

        public MembreBienEchange()
        {
        }

        public MembreBienEchange(int idMembre, int idBien, Membre membre, BienEchange bienEchange, DateTime dateDebEchange, DateTime dateFinEchange, bool? assurance, bool valide)
        {
            IdMembre = idMembre;
            IdBien = idBien;
            Membre = membre;
            BienEchange = bienEchange;
            DateDebEchange = dateDebEchange;
            DateFinEchange = dateFinEchange;
            Assurance = assurance;
            Valide = valide;
        }

        public MembreBienEchange(int idMembreBienEchange, int idMembre, int idBien, Membre membre, BienEchange bienEchange, DateTime dateDebEchange, DateTime dateFinEchange, bool? assurance, bool valide)
        {
            IdMembreBienEchange = idMembreBienEchange;
            IdMembre = idMembre;
            IdBien = idBien;
            Membre = membre;
            BienEchange = bienEchange;
            DateDebEchange = dateDebEchange;
            DateFinEchange = dateFinEchange;
            Assurance = assurance;
            Valide = valide;
        }
    }

}
