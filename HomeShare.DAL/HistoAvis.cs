﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class HistoAvis
    {
      public int IdHistoAvis{ get; set; }
      public int IdAvis  { get; set; }
      public DateTime DateAvis { get; set; }
      public int NoteAvis  { get; set; }
      public int IdMembre { get; set; }
      public int Proprio { get; set; }
      public string TitreBien { get; set; }
      public string VilleBien { get; set; }
      public int NbEchange { get; set; }

        public HistoAvis()
        {
        }

        public HistoAvis(int idAvis, DateTime dateAvis, int noteAvis, int idMembre, int proprio, string titreBien, string villeBien, int nbEchange)
        {
            IdAvis = idAvis;
            DateAvis = dateAvis;
            NoteAvis = noteAvis;
            IdMembre = idMembre;
            Proprio = proprio;
            TitreBien = titreBien;
            VilleBien = villeBien;
            NbEchange = nbEchange;
        }

        public HistoAvis(int idHistoAvis, int idAvis, DateTime dateAvis, int noteAvis, int idMembre, int proprio, string titreBien, string villeBien, int nbEchange)
        {
            IdHistoAvis = idHistoAvis;
            IdAvis = idAvis;
            DateAvis = dateAvis;
            NoteAvis = noteAvis;
            IdMembre = idMembre;
            Proprio = proprio;
            TitreBien = titreBien;
            VilleBien = villeBien;
            NbEchange = nbEchange;
        }
    }
}
