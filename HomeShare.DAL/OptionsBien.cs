﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class OptionsBien
    {
        public int IdOptionsBien { get; set; }
        public int IdOption { get; set; }
        public int IdBien { get; set; }
        public Options Options  { get; set; }
        public BienEchange BienEchange  { get; set; }
        public string Valeur { get; set; }

        public OptionsBien()
        {
        }

        public OptionsBien(int idOption, int idBien, Options options, BienEchange bienEchange, string valeur)
        {
            IdOption = idOption;
            IdBien = idBien;
            Options = options;
            BienEchange = bienEchange;
            Valeur = valeur;
        }

        public OptionsBien(int idOptionsBien, int idOption, int idBien, Options options, BienEchange bienEchange, string valeur)
        {
            IdOptionsBien = idOptionsBien;
            IdOption = idOption;
            IdBien = idBien;
            Options = options;
            BienEchange = bienEchange;
            Valeur = valeur;
        }
    }
}
