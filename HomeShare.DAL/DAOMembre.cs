﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeShare.DAL
{
    public class DAOMembre : DAOCon, IDAO<Membre>
    {
        public Membre create(Membre objet)
        {

            //on prépare notre requête Et il faut qu'on puisse récupérer l'id auto-généré par la DB
            SqlCommand command = connection.CreateCommand();
            //OUTPUT inserted.idUserAccount on recupere ici l'idUserAccount car au moment de la creation on ne connait pas l id 
            string query = "INSERT INTO Membre (nom,prenom,email,pays,telephone,login,password,photoUser,isDeleted) OUTPUT inserted.idMembre VALUES (@nom,@prenom,@email,@pays,@telephone,@login,@password,@photoUser,@isDeleted)";
            command.Parameters.AddWithValue("@nom", objet.Nom);
            command.Parameters.AddWithValue("@prenom", objet.Prenom);
            command.Parameters.AddWithValue("@email", objet.Email);
            command.Parameters.AddWithValue("@pays", objet.IdPays);
            command.Parameters.AddWithValue("@telephone", objet.Telephone);
            command.Parameters.AddWithValue("@login", objet.Login);
            command.Parameters.AddWithValue("@password", objet.Password);
            command.Parameters.AddWithValue("@photoUser", objet.PhotoUser);
            //command.Parameters.AddWithValue("@isDeleted", objet.IsDeleted);
            if (objet.IsDeleted == null) command.Parameters.AddWithValue("@isDeleted", DBNull.Value);
            else command.Parameters.AddWithValue("@isDeleted", objet.IsDeleted);
            command.CommandText = query;
            //On ouvre la connection juste avant l'execution de la commande
            connection.Open();
            //ExecuteScalar exécute une requête et renvoye un OBJET
            objet.IdMembre = (int)command.ExecuteScalar();
            connection.Close();
            return (objet);
        }



        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Membre read(int id)
        {
            SqlCommand command = connection.CreateCommand();

            string query = "SELECT idMembre, login, password FROM Membre WHERE  idMembre= " + id;
            command.CommandText = query;
            command.Parameters.AddWithValue("idContact", id);
            connection.Open();
            SqlDataReader rdr = command.ExecuteReader();
            Membre mbre = null;

            while (rdr.Read())
            {
               mbre = new Membre
                {
                    IdMembre = (int)rdr["idMembre"],
                    Login = (string)rdr["Password"],
                    Password = (string)rdr["email"],
                   
                    

                };

            }
            rdr.Close();
            connection.Close();
            return mbre;
        }


        public bool UserIsValid(string login, string password)
        {


            bool authenticated = false;
            SqlCommand command = connection.CreateCommand();
            string query = string.Format("SELECT * FROM [userAccount] WHERE Login= '{0}' AND Password= '{1}' ", login, password);
            connection.Open();
            //SqlCommand cmd = new SqlCommand(query, connection);
            command.CommandText = query;
            SqlDataReader sdr = command.ExecuteReader();
            authenticated = sdr.HasRows;
            connection.Close();
            return (authenticated);

        }

        public Membre GetUserAccount(string login, string password)
        {

            SqlCommand command = connection.CreateCommand();

            string query = string.Format("SELECT * FROM [userAccount] WHERE login = @login ");
            command.CommandText = query;
            command.Parameters.AddWithValue("@login", login);

            Membre user = null;
            connection.Open();

            SqlDataReader rdr = command.ExecuteReader();


            while (rdr.Read())
            {
                //on passe le paramètre en string et pas un int qui donne l'index de la valeur qu'on veut récupérer => exemple: "idMedecin" au lieu de 6
                //on spécifie au reader qui'il lit des (int), et les paramètres qui doivent renvoyer des sting on leur joint .ToString()
                user = new Membre((int)rdr["idMembre"],  rdr["login"].ToString(), rdr["password"].ToString());


            }

            connection.Close();


            return user;

        }

        public List<Membre> readAll()
        {
       
                //Return list of all userAccount  

                List<Membre> lst = new List<Membre>();


                SqlCommand com = connection.CreateCommand();

                string query = ("SELECT Nom, Prenom, Email, Login FROM Membre ORDER BY idMembre DESC");

                com.CommandText = query;
                connection.Open();
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Membre
                    {
                        IdMembre = Convert.ToInt32(rdr["idMembre"]),
                        Nom= rdr["Nom"].ToString(),
                        Prenom = rdr["Prenom"].ToString(),
                        Email = rdr["Email"].ToString(),
                        Login = rdr["Login"].ToString(),
                       
                    });
                }
                rdr.Close();
                connection.Close();
                return lst;
        
        }

        public Membre update(Membre objet)
        {
            throw new NotImplementedException();
        }
    }
}
